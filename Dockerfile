FROM quay.io/keycloak/keycloak:16.1.0

ADD themes /opt/jboss/keycloak/themes/

RUN curl -L -o /opt/jboss/keycloak/standalone/deployments/keycloak-protocol-cas-16.1.0.jar https://github.com/jacekkow/keycloak-protocol-cas/releases/download/16.1.0/keycloak-protocol-cas-16.1.0.jar
